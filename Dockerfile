FROM python:3.7-alpine3.10

# Setting these environment variables are the same as running
ENV PATH /env/bin:/usr/local/bin/:$PATH
ENV PYTHONUNBUFFERED 1
ENV TZ America/Sao_Paulo
ENV LIBRARY_PATH=/lib:/usr/lib

ADD requirements.txt .

RUN \
    apk add --no-cache postgresql-libs tzdata && \
    apk add --virtual build-deps gcc g++ python3-dev musl-dev jpeg-dev libpng-dev zlib-dev libjpeg freetype-dev &&\
    apk add --no-cache --virtual .build-deps make postgresql-dev libffi-dev libxml2-dev libxslt-dev \
    git cmake gettext-dev libintl &&\
    apk add --no-cache libxslt &&\
    pip install --user --upgrade pip && \
    pip install --user --no-cache-dir --no-deps -r requirements.txt
